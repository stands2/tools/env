# Env

## Getting started

Before cloning the repos make sure create a `stands` dir where you want it to be stored with
```sh
mkdir ./stands
```
and go to the project folder
```sh
cd ./stands
```
You can now run the init script for the project
```sh
curl https://gitlab.com/stands2/tools/env/-/raw/main/init-project.sh | bash
```

and you are good to go

# clone all sub-groups folders
mkdir -p ./{backend,clients,tools,templates}

# clone all backend repos
cd backend
git clone git@gitlab.com:stands2/backend/auth-service.git
git clone git@gitlab.com:stands2/backend/user-service.git
git clone git@gitlab.com:stands2/backend/mailer-service.git

# clone all clients repos
cd ../clients
git clone git@gitlab.com:stands2/clients/desktop.git

# clone all tools repos
cd ../tools
git clone git@gitlab.com:stands2/tools/env.git

# clone all templates repos
cd ../templates
git clone git@gitlab.com:stands2/templates/service-template.git

# clone frontend repos
cd ../
git clone git@gitlab.com:stands2/frontend.git

# open code with stands workspace
code tools/env/stands.code-workspace